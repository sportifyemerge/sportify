
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sportfy - Sport like a local</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="css/animate.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/creative.css" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-top">
 <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">Sportify</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="">Home</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">Sport now</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
        
    <section id="other">
        <div class="container">


<?php
//require_once("./login/source/include/membersite_config.php");

$servername = "localhost";
$DBuser = "root";
$DBpass = "ok";
$DBname = "hackathon";
	// Create connection
mysql_connect("localhost", "root", "ok") or die(mysql_error());
mysql_select_db("hackathon") or die(mysql_error());
$formvars['place'] = $_POST['place'];
$formvars['type'] = $_POST['credit-card'];


$result = mysql_query("SELECT * FROM OFFERS WHERE location='".$formvars['place']."' and type='".$formvars['type']."'") 
or die(mysql_error());  

// keeps getting the next row until there are no more to get
?>

<?php
while($row = mysql_fetch_array( $result )) {
	// Print out the contents of each row into a table
		$resultPhoto = mysql_query("SELECT * FROM OFFER_PHOTOS WHERE offer_id=".$row['offer_id']);
		$photoDetails = mysql_fetch_array( $resultPhoto );
	//	echo "/n photo URL:".$photoDetails['photo_url'];
		$resultAmenities = mysql_query("SELECT * FROM OFFER_AMENITIES WHERE offer_id=".$row['offer_id']);
	//	while($amenityDetails = mysql_fetch_array( $resultAmenities ))
	//		echo "/n amenities:".$amenityDetails['amenities'];

?>
            <div class="row">
                <div class="card flat" style="display: block;">
                <div class="card-content"> 
<div class="row">    
	                    <div class="col-md-4 photo">
	                        <div class="card-image card-200">
								<img alt="Image" class="responsive-img" src=<?php echo $photoDetails['photo_url']; ?>>
			                      
			                      <div class="media-photo media-round">
			                        <img src="https://a0.muscache.com/im/pictures/1d0c0a58-c17c-403a-a371-a58167e01c3f.jpg?aki_policy=profile_medium" alt="Julian from Cocoa Beach" data-pin-nopin="true">
			                      </div>
	                        </div>
	                    </div>
	                    <div class="col-md-4">
                            <h6 class="right"><?php echo $row['offername']; ?></h6>
                        	<?php echo   "<p class='description'>".$row['description']."</p>"; ?>
	                    </div>
	                    <div class="col-md-4">                                
							<a href="/templates/template" class="tag"><img alt="Image" class="responsive-img" src="img/surfing.png"></a>  
							<a href="/templates/template" class="tag"><img alt="Image" class="responsive-img" src="img/surfing.png"></a>  
							<a href="/templates/template" class="tag"><img alt="Image" class="responsive-img" src="img/surfing.png"></a>  
							<a href="/templates/template" class="tag"><img alt="Image" class="responsive-img" src="img/surfing.png"></a>  
							<a href="/templates/template" class="tag"><img alt="Image" class="responsive-img" src="img/surfing.png"></a>  
							<a href="/templates/template" class="tag"><img alt="Image" class="responsive-img" src="img/surfing.png"></a>  
							<a href="/templates/template" class="tag"><img alt="Image" class="responsive-img" src="img/surfing.png"></a>
							<div class="clear"></div>
							<center><a href="visa-direct.php"; class="btn btn-primary btn-xl page-scroll">Book now</a></center>      
	                    </div>
                    </div>
   				</div>
                </div>
            </div> <!-- end row -->
<?php	} ?>
     </div>    	
    </section>
   <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Let's Get In Touch!</h2>
                    <hr class="primary">
                    <p>If you have any question feel free to contact us!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x wow bounceIn"></i>
                    <p>123-456-6789</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
                    <p><a href="mailto:danilo@snowmanlabs.com">danilo@snowmanlabs.com</a></p>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/wow.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/creative.js"></script>
</body>
</html>
